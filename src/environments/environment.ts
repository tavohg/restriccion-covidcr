// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDk-IQSHjxezKzwDW1PlaFPdsXlU5bRJnE',
    authDomain: 'restriccion-covidcr-dev.firebaseapp.com',
    databaseURL: 'https://restriccion-covidcr-dev.firebaseio.com',
    projectId: 'restriccion-covidcr-dev',
    storageBucket: 'restriccion-covidcr-dev.appspot.com',
    messagingSenderId: '575467001691',
    appId: '1:575467001691:web:fb2223596c67698e3eac7e',
    measurementId: 'G-2D7S6DB3EK'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
