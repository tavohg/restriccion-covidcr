import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Restriction } from '../models/restriction';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  constructor(private db: AngularFirestore, private analytics: AngularFireAnalytics) {}

  getRestrictionByDate(selectedDate: Date): Observable<any> {
    return this.db
      .collection('restrictions', (ref) =>
        ref.where('date', '==', selectedDate)
      )
      .valueChanges();
  }

  getRestrictionsDescByDate(): Observable<any> {
    return this.db
      .collection('restrictions', (ref) => ref.orderBy('date', 'desc'))
      .valueChanges();
  }

  addPlateSubscriber(email: string, plate: string) {
    this.db.collection('subscribers').add({
      email,
      plate,
    });
  }

  logCustomAnalyticsEvent(eventName: string) {
    this.analytics.logEvent(eventName);
  }
}
