import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { FirebaseService } from '../shared/firebase.service';
import { Restriction } from '../models/restriction';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  selectedDate: Date;

  startDate = new Date();
  minDate = new Date();
  maxDate: Date;

  consultedRestriction: Restriction;

  isLoading = false;

  noSchedule: boolean;

  dayTimeRestriction = false;
  nightTimeRestriction = false;
  infoLoaded = false;

  canTravelDay: boolean;
  canTravelNight: boolean;

  dayRestrictionMessage: string;
  noDayRestrictionMessage: string;
  nightRestrictionMessage: string;
  noNightRestrictionMessage: string;

  title = 'Suscribirse a notificaciones por correo';

  plateSubscriberForm = new FormGroup({
    email: new FormControl(
      null,
      Validators.compose([Validators.required, Validators.email])
    ),
    plate: new FormControl(
      null,
      Validators.compose([
        Validators.required,
        Validators.pattern('^([0-9]{1,7})$|^(([aA-zZ]{3})+(-?)+([0-9]{3}))$'),
      ])
    ),
  });

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private firebaseService: FirebaseService,
    private notification: NotificationService
  ) {
    this.matIconRegistry.addSvgIcon(
      'logo',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/logo.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'allowed',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/allowed.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'warning',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/warning.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'fatal',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/fatal.svg')
    );
  }

  ngOnInit(): void {
    this.firebaseService.logCustomAnalyticsEvent('home_page_visit');
    this.firstFormGroup = this.formBuilder.group({
      plate: [null, Validators.pattern('^([0-9]){1}$')],
    });
    this.secondFormGroup = this.formBuilder.group({
      selectedDate: [null, Validators.required],
    });
    this.firebaseService
      .getRestrictionsDescByDate()
      .subscribe((orderedDescRestrictions) => {
        this.maxDate = orderedDescRestrictions[0].date.toDate();
      });
  }

  submitConsult() {
    this.firebaseService.logCustomAnalyticsEvent('submited_consult');
    this.selectedDate = this.secondFormGroup.get('selectedDate').value;
    this.isLoading = true;
    this.firebaseService
      .getRestrictionByDate(this.secondFormGroup.get('selectedDate').value)
      .subscribe(async (restriction) => {
        this.consultedRestriction = new Restriction();
        this.consultedRestriction.date = await restriction[0].date.toDate();
        this.consultedRestriction.noSchedule = await restriction[0].noSchedule;
        if (restriction[0].dayCondition) {
          this.consultedRestriction.dayCondition = await restriction[0]
            .dayCondition;
        }
        if (restriction[0].nightCondition) {
          this.consultedRestriction.nightCondition = await restriction[0]
            .nightCondition;
        }
        this.consultedRestriction.dayPlates = await restriction[0].dayPlates;
        if (restriction[0].nightPlates) {
          this.consultedRestriction.nightPlates = await restriction[0]
            .nightPlates;
        }
        this.consultedRestriction.startDayTime = await restriction[0].startDayTime.toDate();
        this.consultedRestriction.endDayTime = await restriction[0].endDayTime.toDate();
        if (restriction[0].startNightTime) {
          this.consultedRestriction.startNightTime = await restriction[0].startNightTime.toDate();
        }
        if (restriction[0].endNightTime) {
          this.consultedRestriction.endNightTime = await restriction[0].endNightTime.toDate();
        }
        // console.log(this.consultedRestriction);
        this.infoLoaded = await this.setRestriction();
        this.isLoading = false;
        setTimeout(() => {
          const restrictionsContainer: HTMLElement = document.getElementById(
            'restrictions'
          );
          restrictionsContainer.scrollIntoView({
            behavior: 'smooth',
            block: 'center',
          });
        }, 100);
      });
  }

  resetRestriction() {
    this.selectedDate = undefined;
    this.consultedRestriction = undefined;
    this.noSchedule = undefined;
    this.dayTimeRestriction = false;
    this.nightTimeRestriction = false;
    this.infoLoaded = false;
    this.isLoading = false;
    this.canTravelDay = undefined;
    this.canTravelNight = undefined;
    this.dayRestrictionMessage = undefined;
    this.noDayRestrictionMessage = undefined;
    this.nightRestrictionMessage = undefined;
    this.noNightRestrictionMessage = undefined;
  }

  async setRestriction(): Promise<boolean> {
    const lastPlateDigit = parseInt(this.firstFormGroup.get('plate').value, 10);
    if (this.consultedRestriction.noSchedule) {
      this.noSchedule = true;
      if (this.consultedRestriction.dayPlates.length > 0) {
        this.dayTimeRestriction = true;
        if (this.consultedRestriction.dayPlates[0] === 10) {
          this.canTravelDay = false;
        } else {
          this.canTravelDay = !this.consultedRestriction.dayPlates.includes(
            lastPlateDigit
          );
        }
        this.canTravelDay
          ? (this.dayRestrictionMessage =
              'Su vehículo SÍ puede circular durante todo el día ' +
              (this.consultedRestriction.dayCondition
                ? this.consultedRestriction.dayCondition
                : '') +
              '.')
          : (this.dayRestrictionMessage =
              'Su vehículo NO puede circular en este día.');
      }
    } else {
      this.noSchedule = false;
      if (this.consultedRestriction.dayPlates.length > 0) {
        this.dayTimeRestriction = true;
        if (this.consultedRestriction.dayPlates[0] === 10) {
          this.canTravelDay = false;
        } else {
          this.canTravelDay = !this.consultedRestriction.dayPlates.includes(
            lastPlateDigit
          );
        }
        this.canTravelDay
          ? (this.dayRestrictionMessage =
              'Su vehículo SÍ puede circular ' +
              (this.consultedRestriction.dayCondition
                ? this.consultedRestriction.dayCondition
                : '') +
              ' desde ' +
              this.consultedRestriction.startDayTime.toLocaleTimeString(
                'es-US'
              ) +
              ' del ' +
              this.consultedRestriction.startDayTime.toLocaleDateString(
                'es-US',
                {
                  month: 'long',
                  day: 'numeric',
                  weekday: 'long',
                }
              ) +
              ' hasta ' +
              this.consultedRestriction.endDayTime.toLocaleTimeString('es-US') +
              ' del ' +
              this.consultedRestriction.endDayTime.toLocaleDateString('es-US', {
                month: 'long',
                day: 'numeric',
                weekday: 'long',
              }) +
              '.')
          : (this.dayRestrictionMessage =
              'Su vehículo NO puede circular desde ' +
              this.consultedRestriction.startDayTime.toLocaleTimeString(
                'es-US'
              ) +
              ' del ' +
              this.consultedRestriction.startDayTime.toLocaleDateString(
                'es-US',
                {
                  month: 'long',
                  day: 'numeric',
                  weekday: 'long',
                }
              ) +
              ' hasta ' +
              this.consultedRestriction.endDayTime.toLocaleTimeString('es-US') +
              ' del ' +
              this.consultedRestriction.endDayTime.toLocaleDateString('es-US', {
                month: 'long',
                day: 'numeric',
                weekday: 'long',
              }) +
              '.');
      } else {
        this.dayTimeRestriction = false;
        this.noDayRestrictionMessage =
          'Su vehículo SÍ puede circular desde ' +
          this.consultedRestriction.startDayTime.toLocaleTimeString('es-US') +
          ' del ' +
          this.consultedRestriction.startDayTime.toLocaleDateString('es-US', {
            month: 'long',
            day: 'numeric',
            weekday: 'long',
          }) +
          ' hasta ' +
          this.consultedRestriction.endDayTime.toLocaleTimeString('es-US') +
          ' del ' +
          this.consultedRestriction.endDayTime.toLocaleDateString('es-US', {
            month: 'long',
            day: 'numeric',
            weekday: 'long',
          }) +
          '.';
      }
      if (this.consultedRestriction.nightPlates.length > 0) {
        this.nightTimeRestriction = true;
        if (this.consultedRestriction.nightPlates[0] === 10) {
          this.canTravelNight = false;
        } else {
          this.canTravelNight = !this.consultedRestriction.nightPlates.includes(
            lastPlateDigit
          );
        }
        this.canTravelNight
          ? (this.nightRestrictionMessage =
              'Su vehículo SÍ puede circular ' +
              (this.consultedRestriction.nightCondition
                ? this.consultedRestriction.nightCondition
                : '') +
              ' desde ' +
              this.consultedRestriction.startNightTime.toLocaleTimeString(
                'es-US'
              ) +
              ' del ' +
              this.consultedRestriction.startNightTime.toLocaleDateString(
                'es-US',
                {
                  month: 'long',
                  day: 'numeric',
                  weekday: 'long',
                }
              ) +
              ' hasta ' +
              this.consultedRestriction.endNightTime.toLocaleTimeString(
                'es-US'
              ) +
              ' del ' +
              this.consultedRestriction.endNightTime.toLocaleDateString(
                'es-US',
                {
                  month: 'long',
                  day: 'numeric',
                  weekday: 'long',
                }
              ) +
              '.')
          : (this.nightRestrictionMessage =
              'Su vehículo NO puede circular desde ' +
              this.consultedRestriction.startNightTime.toLocaleTimeString(
                'es-US'
              ) +
              ' del ' +
              this.consultedRestriction.startNightTime.toLocaleDateString(
                'es-US',
                {
                  month: 'long',
                  day: 'numeric',
                  weekday: 'long',
                }
              ) +
              ' hasta ' +
              this.consultedRestriction.endNightTime.toLocaleTimeString(
                'es-US'
              ) +
              ' del ' +
              this.consultedRestriction.endNightTime.toLocaleDateString(
                'es-US',
                {
                  month: 'long',
                  day: 'numeric',
                  weekday: 'long',
                }
              ) +
              '.');
      } else {
        this.nightTimeRestriction = false;
        this.noNightRestrictionMessage =
          'Su vehículo SÍ puede circular desde ' +
          this.consultedRestriction.startNightTime.toLocaleTimeString('es-US') +
          ' del ' +
          this.consultedRestriction.startNightTime.toLocaleDateString('es-US', {
            month: 'long',
            day: 'numeric',
            weekday: 'long',
          }) +
          ' hasta ' +
          this.consultedRestriction.endNightTime.toLocaleTimeString('es-US') +
          ' del ' +
          this.consultedRestriction.endNightTime.toLocaleDateString('es-US', {
            month: 'long',
            day: 'numeric',
            weekday: 'long',
          }) +
          '.';
      }
    }
    return true;
  }

  /* Function called when the add player button is clicked */
  onNewSubscriber(): void {
    this.firebaseService.logCustomAnalyticsEvent('new_subscriber');
    const subscriptionFormContainer: HTMLElement = document.getElementById(
      'subscription'
    );
    subscriptionFormContainer.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
    });
  }

  initializeFormGroup() {
    this.plateSubscriberForm.setValue({
      email: null,
      plate: null,
    });
  }

  onClear() {
    this.plateSubscriberForm.reset();
    this.initializeFormGroup();
    this.notification.success('Formulario borrado');
  }

  onSubmit() {
    this.firebaseService.addPlateSubscriber(this.plateSubscriberForm.value.email, this.plateSubscriberForm.value.plate);
    this.plateSubscriberForm.reset();
    this.initializeFormGroup();
    this.notification.success('Registro completado, revisa tu correo a partir de mañana.');
  }
}
